package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    int max_size = 20;
    ArrayList<FridgeItem> fridgeItems = new ArrayList<>();

    public int nItemsInFridge(){
        return fridgeItems.size();
    }

    public int totalSize(){
        return max_size;
    }

    public boolean placeIn(FridgeItem item){
        int currentSize = nItemsInFridge(); 
        FridgeItem fridgeItem = item;
        System.out.println(fridgeItem);

        if (currentSize < max_size){
            fridgeItems.add(fridgeItem);
            return true;
        } else {
            return false;
        }
    }

    public void takeOut(FridgeItem item){
        FridgeItem fridgeItem = item;

        if(fridgeItems.contains(fridgeItem)){
            fridgeItems.remove(fridgeItem);
        } else {
            throw new NoSuchElementException();
        }
    }

    public void emptyFridge(){
        fridgeItems.clear();
    }

    public List<FridgeItem> removeExpiredFood(){
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for(int i=0; i < nItemsInFridge(); i++){
            FridgeItem item = fridgeItems.get(i);
            if(item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        for(FridgeItem expiredItem : expiredFood){
            fridgeItems.remove(expiredItem);
        }
        return expiredFood;
    }

}
